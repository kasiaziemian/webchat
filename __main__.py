import socket


from chat import *
from server import *
from helpers import *
from settings import *

def serve_forever():
  www = Chat()
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)    

  s.bind((HOST, PORT))
  s.listen(32) 
  s.settimeout(1)

  while True:
    try:
      conn, conn_addr = s.accept()
      conn.setblocking(1)  
      print(f"connection: {conn_addr}\n")
    except socket.timeout as e:
      continue  

    hs = HTTPServer(www, conn, conn_addr)
    hs.start()


if __name__ == "__main__":
  serve_forever()