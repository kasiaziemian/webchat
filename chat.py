import os
import json
import socket
import threading
from threading import Lock

from helpers import *
from __main__ import *
from settings import *


class Chat():
  def __init__(self):
    self.files = "/templates" 
    self.posts = []
    self.posts_offset = 0
    self.posts_lock = Lock()
    self.posts_limit = 500  
    self.file_cache = {}
    self.file_cache_lock = Lock()
    self.methods = {('GET', '/index.html'): self.main_html,
                    ('GET', '/style.css'): self.main_css,
                    ('GET', '/main.js'): self.main_js,
                    ('POST', '/chat'): self.chat,
                    ('POST', '/posts'): self.posts,}

  def http_request(self, request):
    query = (request['command'], request['path'])
    if query not in self.methods:
      return { 'response_code': (404) }
    return self.methods[query](request)

  def main_html(self, request):
    return self.render_template('templates/index.html')

  def main_css(self, request):
    return self.render_template('templates/style.css')

  def main_js(self, request):
    return self.render_template('templates/main.js')

  def chat(self, request):
    try:
      obj = json.loads(request['data'])
    except ValueError:
      return {'response_code': (400)}

    if type(obj) is not dict:
      return {'response_code': (400)}
    
    if 'text' not in obj:
      return {'response_code': (400)}

    text = obj['text']
    if type(text) is not str and type(text) is not unicode:
      return { 'response_code': (400)}

    user = request['server']

    with self.posts_lock:
      if len(self.posts) > self.posts_limit:
        self.posts.pop(0)
        self.posts_offset += 1
      self.posts.append((user, text))

    print(f"(user, text)n")

    return {'response_code': (200)}
 
  def posts(self, request):
    try:
      obj = json.loads(request['data'])
    except ValueError:
      return {'response_code': (400)}

    if type(obj) is not dict or 'end_post' not in obj:
      return {'response_code': (400)}

    end_post = obj['end_post']
    if type(end_post) is not int:
      return {'response_code': (400)}

    with self.posts_lock:
      end_post -= self.posts_offset
      if end_post < 0:
        end_post = 0
      posts = self.posts[end_post:]
      end_post_new = self.posts_offset + len(self.posts)


    data = json.dumps({"end_post": end_post_new, "posts": posts})
    
    return {'response_code': (200),'headers': [('content-type', 'application/json'),],
        'data': data}

  def render_template(self, filename):
    file_entension = os.path.splitext(filename)[1]
    mime_type = {
        '.html': 'text/html',
        '.css': 'text/css',
        '.js': 'application/javascript',
        }.get(file_entension.lower())

    try:
      mtime = os.stat(filename).st_mtime
    except:  
      return {'response_code': (404)}
    try:
      with open(filename, 'rb') as f:
        data = f.read()
        mtime = os.fstat(f.fileno()).st_mtime 
    except IOError as e:
      print(f"File {filename} not found\n")
      return {'response_code': (404)}

    return {'response_code': (200),'headers': [('content-type', mime_type),],'data': data}
 