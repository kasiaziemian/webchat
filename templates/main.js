$(function() {
  $('#chat-input')
    .focus()
    .keypress(function(ev) {
    if (ev.which != 13) {
      return;
    }
    ev.preventDefault();
    var text = $(this).val();
    $(this).val('');
    var text_json = JSON.stringify({
      "text": text
    });
    $.ajax({
      url: '/chat',
      type: 'POST',
      data: text_json,
      async: true
    });
  });
  var end_post = -1;
  var chat = $("#chat-text");
  function checkForNewMessages() {
    var request_json = JSON.stringify({
      "end_post": end_post
    });

    $.ajax({
      url: '/posts',
      type: 'POST',
      data: request_json,
      dataType: 'json',
      async: true,
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("Failed to fetch new messages: " + errorThrown);
        window.setTimeout(checkForNewMessages, 1000);        
      },
      success: function(data) {
        end_post = data["end_post"];
        data["posts"].forEach(function(cv, idx, arr) {
          var person = cv[0];
          var text = cv[1];
          var chat_line = $('<p/>');
          $('<span/>', {
            text: '<' + person + '> '
          }).addClass("person").appendTo(chat_line);
          $('<span/>', {
            text: text
          }).addClass("text").appendTo(chat_line);          
          chat_line.appendTo(chat)
        });

        chat.animate({ scrollTop: chat[0].scrollHeight }, 500);
        window.setTimeout(checkForNewMessages, 1000);        
      },
    });
  }
  checkForNewMessages();
});