import socket
import threading

from chat import *
from helpers import *
from __main__ import *
from settings import *


class HTTPServer(Thread):
  def __init__(self, www, sock, sock_addr):
    super(HTTPServer, self).__init__()    
    self.socket = sock
    self.s_addr = sock_addr
    self.www = www
    
  def parse_request(self):
    data = recv_until_character(self.socket, '\r\n\r\n')
    if not data:
      return None

    http_response = data.split('\r\n')

    words = http_response.pop(0).split(' ')
    if len(words) != 3:
      return None

    command, path, version = words

    headers = {}
    for http_res in http_response:
      tokens = http_res.split(':', 1)
      if len(tokens) != 2:
        continue

      header_name = tokens[0].strip().lower()
      header_value = tokens[1].strip()
      headers[header_name] = header_value

    if command == 'POST':
      try:
        data_length = int(headers['content-length'])
        data = recv_bytes(self.socket, data_length)
      except KeyError as e:
        data = recv_until_disconnect(self.socket)
      except ValueError as e:
        return None      
    else:
      data = None

    key_words = {
        "command": command,
        "path": path,
        "headers": headers,
        "data": data,
        "server": self.s_addr[0],
        "client_port": self.s_addr[1]
        }

    return key_words

  def response(self, code):
    http_response = []
    http_response.append(f"HTTP/1.1 {code['response_code']}")

    http_response.append("server: example")
    if 'data' in code:
      http_response.append(f"content-length: {len(code['data'])}")
    else:
      http_response.append("content-length: 0")

    if 'headers' in code:
      for header in code['headers']:
        http_response.append(f"{header}")
    http_response.append(' ')

    if 'data' in code:
      http_response.append(code['data'])
        
    converted_http_response = []
    for http_res in http_response:
      if type(http_res) is bytes:
        converted_http_response.append(http_res)
      else:
        converted_http_response.append(bytes(http_res, 'utf-8'))
    http_response = converted_http_response
    
    self.socket.sendall(b'\r\n'.join(http_response))

  def client_request(self):
    request = self.parse_request()
    if not request:
      print(f"{self.s_addr} Disconnecting.\n" )
      return

  
    print(f"{(self.s_addr[0], self.s_addr[1], request['path'])} requested\n"  )
    code = self.www.http_request(request)
    self.response(code)

  def run(self):
    self.socket.settimeout(3) 
    try:
      self.client_request()
    except socket.timeout as e:
      if DEBUG:
        print(f"{self.s_addr} timed out.")
    self.socket.shutdown(socket.SHUT_RDWR)
    self.socket.close()


