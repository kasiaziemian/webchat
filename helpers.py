import socket

from chat import *
from __main__ import *

def recv_until_character(sock, character):
  character = [bytes(ch, 'utf-8') for ch in character]
  get_all_bytes = []
  end_bytes = [None] * len(character)

  while end_bytes != character:
    next_byte = sock.recv(1)
    if not next_byte:
      return ''  
    get_all_bytes.append(next_byte)
    end_bytes.pop(0)
    end_bytes.append(next_byte)

  get_all_bytes = b''.join(get_all_bytes)
  return str(get_all_bytes, 'utf-8')

def recv_bytes(sock, b):
  data_bytes = []
  bytes_lenght = 0
  while bytes_lenght != b:
    latest_bytes = sock.recv(b - bytes_lenght)
    if not latest_bytes:
      return None
    data_bytes.append(latest_bytes)
    bytes_lenght += len(latest_bytes)

  data_bytes = b''.join(data_bytes)
  return str(data_bytes, 'utf-8')

def recv_until_disconnect(sock):
  data_bytes = []
  while True:
    latest_bytes = sock.recv(2048)
    if not latest_bytes:
      data_bytes = b''.join(data_bytes)
      return str(data_bytes, 'utf-8')      
    data_bytes.append(latest_bytes)
